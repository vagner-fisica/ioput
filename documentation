IOput(C) C++ library (class) - Version 1.2 20/03/2015

 * This file is the IOput C++ class documentation
 *
 * Copyright (C) 2014 - Vagner Bessa
 *
 * IOput is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * IOput is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the
 * GNU Lesser General Public License
 * along with IOput. If not, see <http://www.gnu.org/licenses/>.

INTRODUCTION
======================
IOput is a C++ library first desgined by Vagner Bessa.

The main purpose of the present library is to provide in a 
cumsomized way methods to either load or save numerical data
in/out of a text file. For example, the IOput class has a
method called 'save_matrix'. Such method can be use to store
data from a 2D array (or a 2D data mapped into a linear array)
intoa file extension '.dat' (default extension) in the form:

a11 a12 a13 ... a1n
a21 a22 a23 ... a2n
.     .          .
.        .       .
.           .    .
am1 am2 am3 ... amn

The same type of data can be loaded using 'load_matrix' method.

In order to use IOput library in your code, please follow instructions at the README file, or in Wiki page of the source repository at

https://bitbucket.org/vagner-fisica/ioput.git

CONSTRUCTORS
=======================

There are 3 constructors:
  default  : IOput ()
  main     : IOput (string fname)
  secondary: Ioput (const char * fname)
  
    Where fname is a string which is supposed to be the container for all in or outputs.
    Example:
     ____________________
     #include "ioput.h"
     #include <cstring>
     
     int main(){
      string fname = "wfolder"
      
      OIput fHandler1;
      OIput fHandler2(fname);
      OIput fHandler3("anotherWFolder");
     }
     _____________________
     
    Above code creates two directory, at current working directory:
      'wfolder'
    and
      'anotherWFolder'
    due to objects 'fHandler2' and 'fHandler3', respectively. Calling default constructor does not create any dir. But all 3 constructors set the default files extensions to ".dat".

WORKING DIRECTORY AND EXTENSION
========================

The main and secondary constructors creates a directory to work on all inputs and outputs, by suppling the directory's name as a string into their arguments. Therefore is it possible to modify the working directory by calling the method:
  
  void change_cwd(string new_dir_name)
  
Then the default working directory is set to 'new_dir_name'.

Every input or output must have extension '.dat'. Otherwise, the default extension can be changed by calling:

  void change_ext(string new_ext)
  
#TO BE CONTINUED...    

