/*
 ******************************
 * ioput.h
 * This file is part of IOput C++ class
 *
 * Copyright (C) 2014 - Vagner Bessa
 *
 * IOput is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * IOput is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the
 * GNU Lesser General Public License
 * along with IOput. If not, see <http://www.gnu.org/licenses/>.
 ******************************
 * IOput C++ class's
 *	- Purpose:
 *     Handle in/out data files in a very
 *			especific way.
 *
 *	- Info:
 *     This class is guaranteed by the developer to
 *		 work if, and only if compiled together with
 *		 source file (ioput.cpp) which is in the root
 *		 directory of the IOclass source code, which 
 *		 can be found at:
 *		 https://vagner-fisica@bitbucket.org/vagner-fisica/ioput.git
 *		 Please check README to see more details.
 *******************************
*/


#ifndef IOPUT_H
#define IOPUT_H

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <sstream>
#include <fstream>
#include <cmath>
#include <algorithm>

#include <string>

#include <stdlib.h>

using std::string;
using std::ifstream;
using std::ofstream;
using std::max;
using std::sqrt;
#define string_s std::stringstream

template <class type>
class IOput{

	public:
//------------------
//Public members:

//------------------
//Public methods:
		IOput();
		IOput(const char*);
		IOput(string);
		
		string cwd();
		string full_cwd();
		void change_cwd(string);
		void change_ext(string);
		
		void save_x(type,
					string,
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
					
		void save_x(type *,
					int,
					string,
					string delimiter = "\n",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
					
		void save_xy(type, type,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
					
		void save_xy(type *, type *,
					int,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_xyz(type, type, type,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_xyz(type *, type *, type*,
					int,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_matrix(type **,
					int, int,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_matrix(type **,
					int, int,
					int, int,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_matrix(type *,
					int, int,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
					
		void save_matrix(type *,
					int, int,
					int, int,
					int,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
								
		void save_vector(type *,
					type **, type **,
					int, int,
					type,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_vector(type *,
					type **, type **,
					int, int,
					int, int,
					type,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
					
		void save_vector(type *,
					type *, type *,
					int, int,
					type,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);

		void save_vector(type *,
					type *, type *,
					int, int,
					int, int,
					int,
					type,
					string,
					string delimiter = "\t",
					int idx = -1,
					int nzeros = -1,
					bool append = false,
					bool new_sub_folder = false);
		
		void read_line(string &,
		               string,
		               char comment = '#',
		               bool close = true);

		string read_line(string,
		               char comment = '#',
		               bool close = true);
		
		void load_x(type *,
		            int,
		            string);

		void load_xy(type *, type *,
		            int,
		            string);
		            
		void load_xyz(type *, type *, type *,
		            int,
		            string);

		void load_matrix(type **,
		            int, int,
		            string);

		void load_matrix(type **,
		            int, int,
		            int, int,
		            string);

		void load_matrix(type *,
		            int, int,
		            string);

		void load_matrix(type *,
		            int, int,
		            int, int,
		            int,
		            string);
		void load_matrix(type *,
		            int, int,
		            int, int,
		            int, int,
		            int, int,
		            string);
		            
		void close_inf();               											
		void format_out_data(string, int);
		
		string lzeros(int, int);
		
	private:	
//------------------
//Private members:
		string *cwd_;
		ofstream outf;
		ifstream inf;
		
		string ext;

//------------------
//Private methods:
		void mkdir(string );
		void mk_subdir(string& );
		void def_par_handler(string&, int, int, bool, bool);
};
#endif
